import os
import ssl

import requests
from aiohttp import web
from lxml import html


class CatalogItem():
    def __init__(self):
        self.id = ""
        self.recommend = False
        self.goods_sale = False
        self.title = ""
        self.image = ""
        self.capacity = ""
        self.starting_current = ""
        self.polarity = ""
        self.dimensions = ""
        self.warranty = ""
        self.exchange_price = ""
        self.price = ""


class CarBattery():
    def __init__(self):
        self.title = ""
        self.subtitle = ""
        self.image = ""
        self.exchange_price = ""
        self.price = ""
        self.status = ""
        self.class_battery = ""
        self.agm = ""
        self.efb = ""
        self.punch_frame = ""
        self.ag = ""
        self.voltage = ""
        self.type = ""
        self.standard_size = ""
        self.length = ""
        self.width = ""
        self.height = ""
        self.polarity = ""
        self.terminal_type = ""
        self.bottom_mount = ""
        self.serviceability = ""
        self.warranty = ""
        self.brand = ""
        self.manufacturer = ""
        self.production_region = ""
        self.capacity = ""
        self.starting_current = ""
        self.product_code = ""
        self.shop = list()



base_url = 'https://www.avtomotiv.ru'
routes = web.RouteTableDef()
dir = os.path.dirname(os.path.abspath(__file__))


@routes.get('/api/v1/car-batteries-catalog')
async def handle(request):
    params = request.rel_url.query

    filter = ""

    try:
        city = params['city']
    except KeyError:
        city = 'omsk'
    try:
        page = params['p']
    except KeyError:
        page = 1
    try:
        from_capacity = int(params['from-capacity'])
    except KeyError:
        from_capacity = 0
    try:
        to_capacity = int(params['to-capacity'])
    except KeyError:
        to_capacity = 250
    try:
        from_starting_current = int(params['from-starting-current'])
    except KeyError:
        from_starting_current = 0
    try:
        to_starting_current = int(params['to-starting-current'])
    except KeyError:
        to_starting_current = 1600

    if from_capacity != 0 or to_capacity != 250:
        filter = "%sakb_capacity-from-%s-to-%s/" % (filter, from_capacity, to_capacity)
        filter = filter.replace("/all/", "/")
    if from_starting_current != 0 or to_starting_current != 1600:
        filter = "%sakb_starting_current-from-%s-to-%s/" % (filter, from_starting_current, to_starting_current)
        filter = filter.replace("/all/", "/")
    if filter != "":
        filter = "filter/%sapply/" % (filter)

    if city == 'moscow':
        url = (base_url + '/catalog/avto-akkumulyatory/%spage%s').replace('www', city) % (filter,
                                                                                                      page)
    elif city == 'novosibirsk':
        url = base_url + '/catalog/avto-akkumulyatory/%spage%s/?new_city=%s' % (filter, page, city)
    else:
        url = base_url + '/%s/catalog/avto-akkumulyatory/%spage%s' % (city, filter, page)
    print(url)
    r = requests.get(url)

    catalog_items = list()

    raw_string = html.fromstring(r.content)
    try:
        cur_page = raw_string.xpath(".//*[@class='pagination-el pagination-el--active']")[0].text.strip()
    except IndexError:
        cur_page = "1"
    if cur_page != str(page):
        None
    else:
        raw_html = raw_string.xpath(".//*[@class='goods new-goods']")

        for item in raw_html:
            catalog_item = CatalogItem()

            temp = item.xpath("a[2]")[0]
            id = str(temp.get("href")).split('/')[-2]
            catalog_item.id = id
            # print(id)
            recomend = False
            if len(item.xpath("span")) > 0:
                recomend = True
            catalog_item.recommend = recomend
            # print(recomend)
            goods_sale = False
            if len(item.xpath("div[@class='goods-sale']")) > 0:
                goods_sale = True
            catalog_item.goods_sale = goods_sale
            # print(goods_sale)
            title = item.xpath("a[2]/div")[0]
            catalog_item.title = title.text.strip()
            # print(title.text)
            temp = item.xpath("a[1]")[0]
            image = base_url + str(temp.get("style"))[str(temp.get("style")).find("(") + 1:-1]
            catalog_item.image = image
            # print(image)

            temp = item.xpath("div[@class='goods-descr']/div[@class='book-row']")
            for sub_item in temp:
                val = sub_item.xpath("div[@class='book-row__value']")[0]
                if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Емкость (Ач)':
                    catalog_item.capacity = str(val.text).strip()
                if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Стартовый ток (А)':
                    catalog_item.starting_current = str(val.text).strip()
                if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Полярность':
                    catalog_item.polarity = str(val.text).strip()
                if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Габариты':
                    catalog_item.dimensions = str(val.text).strip()
                if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Гарантия (мес.)':
                    catalog_item.warranty = str(val.text).strip()

            # capacity = item.xpath("div[@class='goods-descr']/div[1]/div[3]")[0]
            # catalog_item.capacity = capacity.text
            # # print(capacity.text)
            # starting_current = item.xpath("div[@class='goods-descr']/div[2]/div[3]")[0]
            # catalog_item.starting_current = starting_current.text
            # # print(starting_current.text)
            # polarity = item.xpath("div[@class='goods-descr']/div[3]/div[3]")[0]
            # catalog_item.polarity = polarity.text
            # # print(polarity.text)
            # dimensions = item.xpath("div[@class='goods-descr']/div[4]/div[3]")[0]
            # catalog_item.dimensions = dimensions.text
            # # print(dimensions.text)
            # warranty = item.xpath("div[@class='goods-descr']/div[5]/div[3]")[0]
            # catalog_item.warranty = warranty.text
            # # print(warranty.text)
            temp = item.xpath("div[@class='goods-price']/div[@class='goods-price__text']/div[@class='goods-price__val']")
            exchange_price = ""
            if len(temp) > 0:
                exchange_price = temp[0].text.strip()
            catalog_item.exchange_price = exchange_price
            # print(exchange_price)
            temp = item.xpath("div[@class='goods-footer']/div[1]/div[2]")
            price = ""
            if len(temp) > 0:
                price = temp[0].text.strip()
            catalog_item.price = price
            # print(price)
            catalog_items.append(catalog_item.__dict__)

    return web.json_response(catalog_items)

    # raw_string = html.fromstring(r.content)
    # raw_html = raw_string.xpath(".//*[@id='recent']/div/div/*/div[2]/div[2]/a[1]")  # выбор постов
    #
    # posts = list()
    #
    # for item in raw_html:  # обработка элемента raw_html
    #     try:
    #         ref = str(item.get("href"))  # получение ссылки на пост
    #         posts.append(ref)  # добавление ссылки в список post
    #         # f.write(id[1:id.index('fref') - 1] + '\n')
    #         # f.write(item.get("href") + '\n')
    #         # print(item.get("href"))
    #         print(ref)
    #     except:
    #         print('1')
    #         continue
    # return web.Response(body=data,content_type='text/json')
    # return web.Response(body=r.text, content_type='text/html')


@routes.get('/api/v1/get-cities')
async def handle(request):
    r = requests.get(base_url)

    cities = list()

    raw_string = html.fromstring(r.content)
    raw_html = raw_string.xpath(".//*[@class='cityList-wrap']/*/li/a")

    for item in raw_html:
        city = str(item.get("href")).split('/')[-2].split('.')[0]
        if city == 'www':
            city = 'novosibirsk'

        city_view = str(item.text.strip())

        cities.append(dict(city=city, cityView=city_view))
        # print(city)

    return web.json_response(cities)


@routes.get('/api/v1/car-battery')
async def handle(request):
    params = request.rel_url.query

    try:
        city = params['city']
    except KeyError:
        city = 'omsk'
    try:
        id = params['id']
    except KeyError:
        id = 0

    if city != 'moscow':
        url = base_url + '/%s/catalog/avto-akkumulyatory/%s/' % (city, id)
    else:
        url = (base_url + '/catalog/avto-akkumulyatory/%s/').replace('www', city) % (id)

    print(url)
    r = requests.get(url)
    if r.status_code == 404:
        return web.json_response({})

    car_battery = CarBattery()

    raw_string = html.fromstring(r.content)
    car_battery.title = raw_string.xpath(".//main/div[1]/h1")[0].text.strip()
    try:
        car_battery.subtitle = raw_string.xpath(".//main/div[1]/h1/span")[0].text.strip()
    except IndexError:
        None
    try:
        car_battery.image = base_url + raw_string.xpath(".//main/div[2]/div[1]/div[1]/a")[0].get("href")
    except IndexError:
        None
    try:
        car_battery.exchange_price = str(raw_string.xpath(".//div[@class='product-cont-price__val']")[0].text).strip()
    except IndexError:
        None
    try:
        car_battery.price = str(raw_string.xpath(".//div[@class='product-cont-footer__price-true']")[0].text).strip()
    except IndexError:
        None

    item = raw_string.xpath(".//div[@class='product-decr__col']/div[@class='book-row']")
    for sub_item in item:
        try:
            val = sub_item.xpath("div[@class='book-row__value']/a")[0]
        except IndexError:
            val = sub_item.xpath("div[@class='book-row__value']")[0]
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Статус':
            car_battery.status = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Класс':
            car_battery.class_battery = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'AGM':
            car_battery.agm = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'EFB':
            car_battery.efb = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'PUNCH FRAME':
            car_battery.punch_frame = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Ag+':
            car_battery.ag = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Вольтаж (В)':
            car_battery.voltage = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Тип':
            car_battery.type = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Типоразмер':
            car_battery.standard_size = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Длина (мм)':
            car_battery.length = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Ширина (мм)':
            car_battery.width = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Высота (мм)':
            car_battery.height = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Полярность':
            car_battery.polarity = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Тип клемм':
            car_battery.terminal_type = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Нижнее крепление':
            car_battery.bottom_mount = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Обслуживаемость':
            car_battery.serviceability = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Гарантия (мес.)':
            car_battery.warranty = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Бренд':
            car_battery.brand = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Производитель':
            car_battery.manufacturer = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Регион производства':
            car_battery.production_region = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Емкость (Ач)':
            car_battery.capacity = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Стартовый ток (А)':
            car_battery.starting_current = str(val.text).strip()
        if sub_item.xpath("div[@class='book-row__title']")[0].text.strip() == 'Код товара':
            car_battery.product_code = str(val.text).strip()

    item = raw_string.xpath(".//div[@class='bay-location__list-el']")
    for sub_item in item:
        shop_title = sub_item.xpath("div[@class='bay-location-info']/div[@class='bay-location-info-text']/a")[
            0].text.strip()
        shop_tel = sub_item.xpath("div[@class='bay-location-info']/div[@class='bay-location-info-phone']/a")[
            0].text.strip()
        shop_status = \
        sub_item.xpath("div[@class='bay-location-condition']/div[@class='condition']/div[@class='condition-text']")[
            0].text.strip()
        car_battery.shop.append(dict(shop_title=shop_title, shop_phone=shop_tel, shop_status=shop_status))

    return web.json_response(car_battery.__dict__)
    # return web.json_response(cities)

if __name__ == '__main__':
    app = web.Application()
    app.add_routes(routes)

    ssl_context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
    ssl_context.load_cert_chain('%s/cert.pem' % dir, '%s/key.pem' % dir)
    web.run_app(app, port=443, host='0.0.0.0', ssl_context=ssl_context)
    # web.run_app(app, port=8443, host='0.0.0.0', ssl_context=ssl_context)
